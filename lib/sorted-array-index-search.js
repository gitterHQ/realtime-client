'use strict';

function sortedArrayIndexSearch(array, comparator, value) {
  let low = 0;
  let high = array.length;
  while (low < high) {
    const mid = Math.floor((low + high) / 2);
    const current = array[mid];

    if (comparator(current, value) < 0) {
      low = mid + 1;
    } else {
      high = mid;
    }
  }

  return low;
}

module.exports = sortedArrayIndexSearch;
