'use strict';

const backboneUrlResolver = require('backbone-url-resolver');
const log = require('loglevel');
const _ = require('underscore');
const Backbone = require('backbone');
const defaultContextModel = require('./default-context-model');
const debug = require('debug-proxy')('grc:template-subscription');

function TemplateSubscription(client, options) {
  this.options = options;
  this.client = client;
  this._subscribed = false;
  this._subscription = null;

  if (!options.onMessage) throw new Error('onMessage is required');

  if (options.urlModel) {
    this.urlModel = options.urlModel;
  } else {
    this.urlModel = this._getUrlModel(options);
  }

  this._subscribe();
}

_.extend(TemplateSubscription.prototype, Backbone.Events, {
  _subscribe(/* options */) {
    if (this._subscribed) return;
    this._subscribed = true;

    this._registerForSnapshots();

    const url = this.url();

    this.listenTo(this.urlModel, 'change:url', function(model, newChannel) {
      // jshint unused:true
      if (newChannel) {
        this._resubscribe(newChannel);
      } else {
        this._unsubscribe();
      }
    });

    if (!url) return;
    this._resubscribe(url);
  },

  cancel() {
    if (!this._subscribed) return;
    this._subscribed = false;

    // Stop listening to model changes....
    this.stopListening(this.contextModel);
    this._unsubscribe();
    this._deregisterForSnapshots();
  },

  _unsubscribe() {
    const subscription = this._subscription;

    const channel = this._channel;
    this._channel = null;
    if (!subscription) return;

    debug('Unsubscribe: channel=%s', channel);
    subscription.unsubscribe().catch(err => {
      debug('Error unsubscribing from %s: %s', channel, (err && err.stack) || err);
    });
    this._subscription = null;

    this.trigger('unsubscribe');
  },

  _registerForSnapshots() {
    if (this._snapshotsRegistered) return;
    // Do we need snapshots?
    const { options } = this;
    if (!options.getSnapshotState && !options.handleSnapshot && !options.getSubscribeOptions)
      return;
    this._snapshotsRegistered = true;
    // Subscribe to snapshots to the `null` channel so that we
    // can handle snapshots when the channel changes name
    this.client.registerSnapshotHandler(null, this);
  },

  _deregisterForSnapshots() {
    this.client.deregisterSnapshotHandler(null, this);
    this._snapshotsRegistered = false;
  },

  _resubscribe(channel) {
    const oldChannel = this._channel;
    debug('Resubscribe %s (from %s)', channel, oldChannel);

    this._unsubscribe();
    this._channel = channel;

    this.trigger('resubscribe', channel);

    const subscription = (this._subscription = this.client.subscribe(
      channel,
      this.options.onMessage,
    ));

    this._subscription.bind(this).catch(function(err) {
      log.error(`template-subscription: Subscription error for ${channel}`, err);
      this.trigger('subscriptionError', channel, err);

      if (subscription === this._subscription) {
        this._subscription = null;
      }
    });
  },

  // Note that this may be overridden by child classes
  url() {
    return this.urlModel.get('url');
  },

  _getUrlModel(options) {
    const url = _.result(options, 'urlTemplate');
    let contextModel = _.result(options, 'contextModel');

    if (!contextModel) {
      contextModel = defaultContextModel(this.client);
    }

    return backboneUrlResolver(url, contextModel);
  },

  getSnapshotStateForChannel(snapshotChannel) {
    // Since we subscribed to all snapshots, we need to ensure that
    // the snapshot is for this channel
    if (snapshotChannel !== this.urlModel.get('url')) return;

    if (this.options.getSnapshotState) {
      return this.options.getSnapshotState(snapshotChannel);
    }
  },

  getSubscribeOptions(snapshotChannel) {
    if (snapshotChannel !== this.urlModel.get('url')) return;

    if (this.options.getSubscribeOptions) {
      return this.options.getSubscribeOptions(snapshotChannel);
    }
  },

  handleSnapshot(snapshot, snapshotChannel) {
    // Since we subscribed to all snapshots, we need to ensure that
    // the snapshot is for this channel
    if (snapshotChannel !== this.urlModel.get('url')) return;

    if (this.options.handleSnapshot) {
      return this.options.handleSnapshot(snapshot, snapshotChannel);
    }
  },
});

module.exports = TemplateSubscription;
