'use strict';

const LiveCollection = require('./live-collection');
const RoomModel = require('./room-model');

module.exports = LiveCollection.extend({
  model: RoomModel,
  urlTemplate: '/v1/user/:userId/rooms',
  initialize(models, options) {
    // jshint unused:true
    this.userId = options.userId;
    this.listenTo(this, 'change:favourite', this.reorderFavs);
    this.listenTo(this, 'change:lastAccessTime change:lurk', this.resetActivity);
  },

  resetActivity(model) {
    if (model.changed.lastAccessTime && model.get('lastAccessTime')) {
      model.unset('activity');
    }

    if (model.changed.lurk && !model.get('lurk')) {
      model.unset('activity');
    }
  },

  reorderFavs(model) {
    /**
     * We need to do some special reordering in the model of a favourite being positioned
     * This is to mirror the changes happening on the server
     * @see recent-room-service.js@addTroupeAsFavouriteInPosition
     */

    /* This only applies when a fav has been set */
    if (!model.changed || !model.changed.favourite || this.reordering) {
      return;
    }

    this.reordering = true;

    const { favourite } = model.changed;

    const forUpdate = this.map(room => ({ id: room.id, favourite: room.get('favourite') })).filter(
      room => room.favourite >= favourite && room.id !== model.id,
    );

    forUpdate.sort((a, b) => a.favourite - b.favourite);

    let next = favourite;
    for (let i = 0; i < forUpdate.length; i++) {
      const item = forUpdate[i];

      if (item.favourite > next) {
        forUpdate.splice(i, forUpdate.length);
        break;
      }

      item.favourite++;
      next = item.favourite;
    }

    const self = this;
    for (let j = forUpdate.length - 1; j >= 0; j--) {
      const r = forUpdate[j];
      const { id } = r;
      const value = r.favourite;
      const t = self.get(id);
      t.set('favourite', value, { silent: true });
    }

    delete this.reordering;
  },
});
