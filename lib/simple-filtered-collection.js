'use strict';

const Backbone = require('backbone');
const sortedArrayIndexSearch = require('./sorted-array-index-search');

const nullPredicate = function() {
  return true;
};

const SimpleFilteredCollection = Backbone.Collection.extend({
  // eslint-disable-next-line object-shorthand
  constructor: function(models, options) {
    Backbone.Collection.call(this, models, options);

    if (!options || !options.collection) {
      throw new Error(
        'A valid collection must be passed to a new instance of SimpleFilteredCollection',
      );
    }

    this._collection = options.collection;
    this._filterFn = options.filter || this.filterFn;

    if (options.hasOwnProperty('autoResort')) {
      this.autoResort = options.autoResort;
    }

    this.listenTo(this._collection, 'add', this._onAddEvent);
    this.listenTo(this._collection, 'remove', this._onRemoveEvent);
    this.listenTo(this._collection, 'reset', this._resetFromBase);
    this.listenTo(this._collection, 'change', this._onChangeEvent);

    this._resetFromBase();
  },

  autoResort: false,

  /**
   * Default filter
   */
  filterFn: nullPredicate,

  _applyFilter() {
    // Start off by indexing whats already in the collection
    const newIndex = {};
    const filter = this._filterFn;

    const backingModels = this._collection.models;
    for (let i = 0; i < backingModels.length; i++) {
      const backingModel = backingModels[i];
      const matches = filter(backingModel);
      if (matches) {
        newIndex[backingModel.id || backingModel.cid] = backingModel;
      }
    }

    // Now, figure out what we need to add and remove
    const idsToRemove = [];
    const currentModels = this.models;
    for (let j = currentModels.length - 1; j >= 0; j--) {
      const currentModel = currentModels[j];
      const id = currentModel.id || currentModel.cid;

      if (newIndex.hasOwnProperty(id)) {
        // Was in the filtered collection and is still
        // in the collection
        delete newIndex[id];
      } else {
        idsToRemove.push(id);
      }
    }

    if (idsToRemove.length) {
      this.remove(idsToRemove);
    }

    // Add any new items that are left over
    const idsToAdd = Object.keys(newIndex);
    if (idsToAdd.length) {
      const modelsToAdd = Array(idsToAdd.length);
      for (let k = 0; k < idsToAdd.length; k++) {
        modelsToAdd[k] = newIndex[idsToAdd[k]];
      }

      // And a single add
      this.add(modelsToAdd);
    }

    this.trigger('filter-complete');
  },

  _onModelEvent(event, model, collection, options) {
    // Ignore change events from models that have recently been removed
    if (event === 'change' || event.indexOf('change:') === 0) {
      if (!this._filterFn(model)) return;
    }

    return Backbone.Collection.prototype._onModelEvent.call(
      this,
      event,
      model,
      collection,
      options,
    );
  },

  _onAddEvent(model /* , collection, options */) {
    if (this._filterFn(model)) {
      return this.add(model);
    }
  },

  _onRemoveEvent(model /* , collection, options */) {
    return this.remove(model);
  },

  _resetFromBase() {
    const resetModels = this._collection.filter(this._filterFn);
    return this.reset(resetModels);
  },

  _onChangeEvent(model /* , options */) {
    const { cid } = model;
    const existsInCollection = this.get(cid);
    const matchesFilter = this._filterFn(model);
    if (matchesFilter) {
      if (existsInCollection) {
        // Matches filter and exists in collection...
        if (this.autoResort && this.comparator) {
          if (!this._modelIsSorted(model)) {
            this._readdModelForResort(model);
          }
        }
      } else {
        // Matches filter and does not exist in collection...
        this.add(model);
      }

      return;
    }

    if (!matchesFilter && existsInCollection) {
      this.remove(model);
    }
  },

  /**
   * After a change, is the model still correctly sorted?
   */
  _modelIsSorted(model) {
    const len = this.length;
    if (len < 2) return true;
    const index = this.indexOf(model);

    if (index <= 0) {
      return this.comparator(model, this.models[1]) <= 0;
    }

    if (index === len - 1) {
      return this.comparator(this.models[len - 2], model) <= 0;
    }

    return (
      this.comparator(this.models[index - 1], model) <= 0 &&
      this.comparator(model, this.models[index + 1]) <= 0
    );
  },

  /**
   * Remove and readd the model in the correct position
   */
  _readdModelForResort(model) {
    this.remove(model);
    const newIndex = sortedArrayIndexSearch(this.models, this.comparator, model);
    this.add(model, { at: newIndex });
  },

  setFilter(fn) {
    if (fn === this._filterFn) return;
    if (fn) this._filterFn = fn;
    this._applyFilter();
  },

  getUnderlying() {
    return this._collection;
  },
});

module.exports = SimpleFilteredCollection;
