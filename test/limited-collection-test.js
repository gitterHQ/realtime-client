'use strict';

const assert = require('assert');
const Backbone = require('backbone');
const LimitedCollection = require('../lib/limited-collection');

describe('LimitedCollection', () => {
  let baseCollection;
  let limitedCollection;
  let events;

  beforeEach(() => {
    events = [];
    baseCollection = new Backbone.Collection([], {
      comparator(a, b) {
        return a.get('i') - b.get('i');
      },
    });

    limitedCollection = new LimitedCollection([], {
      collection: baseCollection,
      maxLength: 3,
    });

    limitedCollection.on('add', model => {
      events.push({ type: 'add', i: model.get('i') });
    });

    limitedCollection.on('remove', model => {
      events.push({ type: 'remove', i: model.get('i') });
    });

    limitedCollection.on('reset', () => {
      events.push({ type: 'reset' });
    });

    limitedCollection.on('sort', () => {
      events.push({ type: 'sort' });
    });

    limitedCollection.on('change', model => {
      events.push({ type: 'change', i: model.get('i'), prevI: model.previous('i') });
    });
  });

  it('should handle an empty collection', () => {
    assert.strictEqual(limitedCollection.length, 0);
    assert.deepEqual(events, []);
  });

  it('should handle adds', () => {
    assert.strictEqual(limitedCollection.length, 0);

    baseCollection.add({ i: 0 });
    assert.strictEqual(limitedCollection.length, 1);
    assert.deepEqual(events, [{ type: 'add', i: 0 }, { type: 'sort' }]);

    events = [];
    baseCollection.add({ i: 1 });
    assert.strictEqual(limitedCollection.length, 2);
    assert.deepEqual(events, [{ type: 'add', i: 1 }, { type: 'sort' }]);

    events = [];
    baseCollection.add({ i: 2 });
    assert.strictEqual(limitedCollection.length, 3);
    assert.deepEqual(events, [{ type: 'add', i: 2 }, { type: 'sort' }]);

    events = [];
    baseCollection.add({ i: 3 });
    assert.strictEqual(limitedCollection.length, 3);
    assert.deepEqual(events, []);

    const limitedPluck = limitedCollection.pluck('i');
    assert.deepEqual(limitedPluck, [0, 1, 2]);
  });

  it('should handle duplicate adds', () => {
    const a1 = baseCollection.add({ i: 0 });
    assert.strictEqual(limitedCollection.length, 1);
    assert.deepEqual(events, [{ type: 'add', i: 0 }, { type: 'sort' }]);

    events = [];
    baseCollection.add(a1);
    assert.strictEqual(limitedCollection.length, 1);
    assert.deepEqual(events, []);

    const limitedPluck = limitedCollection.pluck('i');
    assert.deepEqual(limitedPluck, [0]);
  });

  it('should handle removes inside the limit with no more items', () => {
    baseCollection.add({ i: 0 });
    baseCollection.add({ i: 1 });
    const i2 = baseCollection.add({ i: 2 });

    assert.strictEqual(limitedCollection.length, 3);

    events = [];
    baseCollection.remove(i2);
    assert.strictEqual(limitedCollection.length, 2);
    assert.deepEqual(events, [{ type: 'remove', i: 2 }]);

    const limitedPluck = limitedCollection.pluck('i');
    assert.deepEqual(limitedPluck, [0, 1]);
  });

  it('should handle removes inside the limit when theres more items', () => {
    baseCollection.add({ i: 0 });
    baseCollection.add({ i: 1 });
    const i2 = baseCollection.add({ i: 2 });
    baseCollection.add({ i: 3 });

    assert.strictEqual(limitedCollection.length, 3);

    events = [];
    baseCollection.remove(i2);
    assert.strictEqual(limitedCollection.length, 3);
    assert.deepEqual(events, [{ type: 'remove', i: 2 }, { type: 'add', i: 3 }, { type: 'sort' }]);

    const limitedPluck = limitedCollection.pluck('i');
    assert.deepEqual(limitedPluck, [0, 1, 3]);
  });

  it('should handle removes outside the limit', () => {
    baseCollection.add({ i: 0 });
    baseCollection.add({ i: 1 });
    baseCollection.add({ i: 2 });
    const i3 = baseCollection.add({ i: 3 });

    assert.strictEqual(limitedCollection.length, 3);

    events = [];
    baseCollection.remove(i3);
    assert.strictEqual(limitedCollection.length, 3);
    assert.deepEqual(events, []);

    const limitedPluck = limitedCollection.pluck('i');
    assert.deepEqual(limitedPluck, [0, 1, 2]);
  });

  it('should handle resets', () => {
    baseCollection.add({ i: 0 });
    baseCollection.add({ i: 1 });
    baseCollection.add({ i: 2 });
    assert.strictEqual(limitedCollection.length, 3);

    events = [];
    baseCollection.reset();
    assert.strictEqual(limitedCollection.length, 0);
    assert.deepEqual(events, [
      {
        type: 'reset',
      },
    ]);

    const limitedPluck = limitedCollection.pluck('i');
    assert.deepEqual(limitedPluck, []);
  });

  it('should handle populated resets', () => {
    baseCollection.add({ i: 0 });
    baseCollection.add({ i: 1 });
    baseCollection.add({ i: 2 });
    assert.strictEqual(limitedCollection.length, 3);

    events = [];
    baseCollection.reset([{ i: 3 }, { i: 4 }, { i: 1 }, { i: 5 }]);
    assert.strictEqual(limitedCollection.length, 3);
    assert.deepEqual(events, [
      {
        type: 'reset',
      },
    ]);

    const limitedPluck = limitedCollection.pluck('i');
    assert.deepEqual(limitedPluck, [1, 3, 4]);
  });
});
