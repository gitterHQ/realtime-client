'use strict';

const assert = require('assert');
const sortedArrayIndexSearch = require('../lib/sorted-array-index-search');

const intComparator = function(a, b) {
  return a - b;
};
const xComparator = function(a, b) {
  return a.x - b.x;
};

describe('sorted-array-index-search', () => {
  it('should work with int arrays', () => {
    const array = [2, 3, 4, 5, 6, 7, 8];
    assert.strictEqual(sortedArrayIndexSearch(array, intComparator, 1), 0);
    assert.strictEqual(sortedArrayIndexSearch(array, intComparator, 2), 0);
    assert.strictEqual(sortedArrayIndexSearch(array, intComparator, 3), 1);
    assert.strictEqual(sortedArrayIndexSearch(array, intComparator, 8), 6);
    assert.strictEqual(sortedArrayIndexSearch(array, intComparator, 9), 7);
  });

  it('should work with object arrays', () => {
    const array = [2, 3, 4, 5, 6, 7, 8].map(x => ({ x }));

    assert.strictEqual(sortedArrayIndexSearch(array, xComparator, { x: 1 }), 0);
    assert.strictEqual(sortedArrayIndexSearch(array, xComparator, { x: 2 }), 0);
    assert.strictEqual(sortedArrayIndexSearch(array, xComparator, { x: 3 }), 1);
    assert.strictEqual(sortedArrayIndexSearch(array, xComparator, { x: 8 }), 6);
    assert.strictEqual(sortedArrayIndexSearch(array, xComparator, { x: 9 }), 7);
  });
});
